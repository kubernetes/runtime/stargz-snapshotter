# stargz-snapshotter

A helm chart for deploying `stargz-snapshotter` as a `daemonset`. 

This dynamically configures `containerd`, `stargz-snapshotter` and the `kubelet` on each node where `stargz` is enabled.

For more information please consult [upstream](https://github.com/containerd/stargz-snapshotter).
